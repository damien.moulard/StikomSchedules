package business.core;

import java.util.List;

/**
 * This class stores the scheduled classes in a certain day.
 * This class is used by the SchedulePanel to display a Schedule.
 * 
 * It contains the name of the day and the list of its classes.
 * Their slot are stored in a second list: At the index i is stored the slot of the class at the index i in the other list.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class DailySchedule {

	private String day;
	private List<CourseClass> classes;
	private List<String> classesSlot;
	
	/**
	 * DailySchedule constructor
	 * 
	 * @param day
	 * 		The related day name
	 * @param classes
	 * 		The list of classes
	 * @param classesSlot
	 * 		The list of time slots
	 */
	public DailySchedule(String day, List<CourseClass> classes,	List<String> classesSlot) {
		super();
		this.day = day;
		this.classes = classes;
		this.classesSlot = classesSlot;
	}
	
	public String getDay() {
		return day;
	}
	
	public List<CourseClass> getClasses() {
		return classes;
	}
	
	public List<String> getClassesSlot() {
		return classesSlot;
	}
}
