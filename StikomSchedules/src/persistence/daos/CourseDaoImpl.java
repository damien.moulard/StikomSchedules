package persistence.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import persistence.DAOFactory;
import persistence.DAOUtility;
import persistence.daoException.DAOException;
import business.core.Course;

/**
 * The CourseDAO implementation class.
 * 
 * @author Damien Moulard
 * @version 1.0
 * 
 * @see CourseDao
 */
public class CourseDaoImpl implements CourseDao {

	private DAOFactory daoFactory;
	private static final String SQL_SELECT_ALL = "SELECT * FROM Course";

	/**
	 * CourseDaoImpl constructor.
	 *  
	 * @param factory
	 * 		The DAOFactory that provides the database connection
	 * 
	 * @see DAOFactory
	 */
	public CourseDaoImpl(DAOFactory factory) {
		this.daoFactory = factory;
	}
	
	/**
	 * Match the result of a query with a Course business class.
	 * 
	 * @param resultSet
	 * 		The result of the query
	 * @return an instance of Course that matches the query result
	 * @throws SQLException
	 * 
	 * @see Course
	 */
	private static Course match (ResultSet resultSet) throws SQLException {
		Course course = new Course(resultSet.getInt("courseId"), resultSet.getString("courseName"));
		return course;
	}
	
	/**
	 * @see DAOUtility
	 * @see Course
	 * @see CourseDaoImpl#match(ResultSet)
	 */
	@Override
	public List<Course> find() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		Course course;
		ArrayList<Course> courseList = new ArrayList<Course>();
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare the query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_ALL, false);
			
			// Retrieve results
			resultSet = preparedQuery.executeQuery();
			
			while (resultSet.next()) {
				// Match result with a Course class before adding it to the list
				course = match(resultSet);
				courseList.add(course);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			// Close connections
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return courseList;
	}

}
