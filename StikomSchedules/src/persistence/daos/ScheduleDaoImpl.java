package persistence.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.mysql.jdbc.Connection;

import persistence.DAOFactory;
import persistence.DAOUtility;
import persistence.daoException.DAOException;
import business.core.CourseClass;
import business.core.DailySchedule;
import business.core.Schedule;

/**
 * The ScheduleDAO implementation class.
 * 
 * @author Damien Moulard
 * @version 1.0
 *  
 * @see ScheduleDao
 */
public class ScheduleDaoImpl implements ScheduleDao {

	private DAOFactory daoFactory;
	private final String SQL_INSERT_SCHEDULE_LINE = "INSERT INTO Schedule VALUES (?, ?, ?)";
	private final String SQL_SELECT_ALL = "SELECT * FROM Schedule";
	//TODO
	private final String SQL_SELECT_BY_DAY = "SELECT Schedule.courseClassId, TimeSlot.slotStart, Day.dayName"
			+ " FROM Schedule, TimeSlot, Day"
			+ " WHERE Schedule.firstTimeSlotId=TimeSlot.slotId"
			+ " AND Schedule.dayId=Day.dayId"
			+ " AND Schedule.dayId=?"
			+ " ORDER BY TimeSlot.slotId";
	private final String SQL_DELETE_ALL = "DELETE FROM Schedule";
	
	/**
	 * ScheduleDaoImpl constructor
	 * 
	 * @param factory
	 * 		The DAOFactory that provides the database connection
	 * 
	 * @see DAOFactory
	 */
	public ScheduleDaoImpl(DAOFactory factory) {
		this.daoFactory = factory;
	}
	
	/**
	 * Match the result of a query with a Schedule business class
	 * 
	 * @param resultSet
	 * 		Query result
	 * @param classes
	 * 		List of all classes
	 * @return The schedule that matches the query result
	 * @throws SQLException 
	 */
	private static Schedule match(ResultSet resultSet, List<CourseClass> classes) throws SQLException {
		Schedule sc = new Schedule();
		CourseClass courseClass;
		int i;
		int classPosition;
		
		while (resultSet.next()) {
			// Look for matching class, stop if class found, i.e. courseClass not null anymore
			courseClass = null;
			i = 0;
			int classId = resultSet.getInt("courseClassId");
			while (i < classes.size() && courseClass == null) {
				if (classes.get(i).getId() == classId) {
					courseClass = classes.get(i);
				}
				i++;
			}
			
			// Determine class position 
			classPosition = (resultSet.getInt("dayId") - 1) * Schedule.hoursPerDay + resultSet.getInt("firstTimeSlotId") - 1;
			
			// Fill schedule slots
			for (int j = 0; j < courseClass.getDuration(); j++) {
				sc.getSlots().get(classPosition + j).add(courseClass);
			}
			
			// Fill schedule classes hashmap
			sc.getClasses().put(courseClass, classPosition);
		}
		
		return sc;
	}
	
	private static DailySchedule matchDailySchedule(ResultSet resultSet, List<CourseClass> allClasses) throws SQLException {
		CourseClass courseClass;
		ArrayList<CourseClass> dailyClasses = new ArrayList<CourseClass>();
		ArrayList<String> classesSlot = new ArrayList<String>();
		int i;
		
		while (resultSet.next()) {
			// Look for matching class, stop if class found, i.e. courseClass not null anymore
			courseClass = null;
			i = 0;
			int classId = resultSet.getInt("courseClassId");
			while (i < allClasses.size() && courseClass == null) {
				if (allClasses.get(i).getId() == classId) {
					courseClass = allClasses.get(i);
				}
				i++;
			}
			dailyClasses.add(courseClass);
			
			classesSlot.add(resultSet.getString("slotStart"));
		}
		
		// The cursor is after the last row here, so get back to the previous position
		resultSet.previous();
		
		return new DailySchedule(resultSet.getString("dayName"), dailyClasses, classesSlot);
	}
	
	/**
	 * @see ScheduleDaoImpl#clear()
	 * @see DAOUtility
	 * @see Schedule
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void save(Schedule sc) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			Map.Entry pair;
			CourseClass courseClass;
			int classPosition;
			int day;
			int timeslot;
			int status;
			
			Iterator<Entry<CourseClass, Integer>> it = sc.getClasses().entrySet().iterator();

			// For each class of the schedule
			while (it.hasNext()) {
				pair = it.next();
				courseClass = (CourseClass) pair.getKey();
				classPosition = (int) pair.getValue();
				
				// Get day and time slot from the class position 
				day = (classPosition / Schedule.hoursPerDay);
				timeslot = classPosition - (day * Schedule.hoursPerDay);
				
				// Indices in database start at 1
				day++;
				timeslot++;
				
				// Prepare query
				preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_INSERT_SCHEDULE_LINE, false, courseClass.getId(), timeslot, day);
				
				// Execute update
				status = preparedQuery.executeUpdate();
				if (status == 0) {
					throw new DAOException("Can't insert into Schedule table.");
				} else {
					DAOUtility.close(preparedQuery);
				}
			}			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.close(preparedQuery, connection);
		}
		
	}
	
	/**
	 * @see DAOUtility
	 * @see ScheduleDaoImpl#match(ResultSet, List)
	 */
	@Override
	public Schedule find() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		Schedule sc = null;
		
		// Get all classes
		CourseClassDao courseClassDao = daoFactory.getCourseClassDao();
		ArrayList<CourseClass> classes = (ArrayList<CourseClass>) courseClassDao.find();
		
		try {
			// Get connection from factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_ALL, false);
			
			// Execute query
			resultSet = preparedQuery.executeQuery();
			
			if (resultSet.next()) {
				// Re-place the resultSet iterator at the beginning
				resultSet.beforeFirst();
				
				// Match result with a Schedule class
				sc = match(resultSet, classes);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return sc;
	}

	/**
	 * @see DailySchedule
	 * @see DAOUtility
	 */
	@Override
	public DailySchedule findByDay(int dayId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		DailySchedule sc = null;
		
		// Get all classes
		CourseClassDao courseClassDao = daoFactory.getCourseClassDao();
		ArrayList<CourseClass> classes = (ArrayList<CourseClass>) courseClassDao.find();
				
		try {
			// Get connection from factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_BY_DAY, false, dayId);
			
			// Execute query
			resultSet = preparedQuery.executeQuery();
			
			if (resultSet.next()) {
				// Re-place the resultSet iterator at the beginning
				resultSet.beforeFirst();
				
				// Match result with a DailySchedule class
				sc = matchDailySchedule(resultSet, classes);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return sc;
	}
	
	/**
	 * @see DAOUtility
	 */
	@Override
	public void clear() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare the query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_DELETE_ALL, false);
			
			// Execute update
			int status = preparedQuery.executeUpdate();
			if (status == 0) {
				throw new DAOException("Can't clear the Schedule table.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtility.close(preparedQuery, connection);
		}
	}
}
