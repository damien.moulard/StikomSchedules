package persistence.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.core.Schedule;
import business.core.Teacher;
import persistence.DAOFactory;
import persistence.DAOUtility;
import persistence.daoException.DAOException;

import com.mysql.jdbc.Connection;

/**
 * The TeacherDAO implementation class.
 * 
 * @author Damien Moulard
 * @version 1.0
 * 
 * @see TeacherDao
 */
public class TeacherDaoImpl implements TeacherDao {
	
	private DAOFactory daoFactory;
	private static final String SQL_SELECT_ALL = "SELECT * FROM Teacher";
	private static final String SQL_SELECT_SHIFT_BY_TEACHER = "SELECT * FROM WorkingShift WHERE teacherId = ? ORDER BY dayId";
	
	/**
	 * TeacherDaoImpl constructor
	 * 
	 * @param factory
	 * 		The DAOFactory that provides the database connection
	 * 
	 * @see DAOFactory
	 */
	public TeacherDaoImpl(DAOFactory factory) {
		this.daoFactory = factory;
	}
	
	/**
	 * Match the result of a query with a Teacher business class
	 *  
	 * @param resultSet
	 * 		The result of the query
	 * @return an instance of Teacher that matches the query result.
	 * @throws SQLException
	 * 
	 * @see Teacher
	 */
	private static Teacher match (ResultSet resultSet, ArrayList<Integer> workingShifts ) throws SQLException {
		Teacher teacher = new Teacher(resultSet.getInt("teacherId"), resultSet.getString("teacherName"), workingShifts);
		return teacher;
	}

	/** 
	 * @see DAOUtility
	 * @see Teacher
	 * @see TeacherDaoImpl#match(ResultSet, ArrayList)
	 * @see TeacherDaoImpl#findTeacherShifts(int)
	 */
	@Override
	public List<Teacher> find() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		Teacher teacher;
		ArrayList<Teacher> teacherList = new ArrayList<Teacher>();
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare the query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_ALL, false);
			
			// Retrieve results
			resultSet = preparedQuery.executeQuery();
			
			while (resultSet.next()) {
				ArrayList<Integer> workingShifts = (ArrayList<Integer>) findTeacherShifts(resultSet.getInt("teacherId"));

				// Match result with a Teacher class before adding it to the list
				teacher = match(resultSet, workingShifts);
				teacherList.add(teacher);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			// Close connections
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return teacherList;
	}

	/**
	 * @see Teacher#getWorkingShifts()
	 * @see DAOUtility
	 */
	@Override
	public List<Integer> findTeacherShifts(int teacherId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		
		// Create and init a list of integers at 0 
		ArrayList<Integer> shifts = new ArrayList<Integer>();
		for (int i = 0; i < Schedule.daysPerWeek; i++) {
			shifts.add(0);
		}
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare the query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_SHIFT_BY_TEACHER, false, teacherId);
			
			// Retrieve results
			resultSet = preparedQuery.executeQuery();
			
			// For each day, set the teacher working shift
			while (resultSet.next()) {
				shifts.remove(resultSet.getInt("dayId")-1);
				shifts.add(resultSet.getInt("dayId")-1, resultSet.getInt("shift"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			// Close connections
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return shifts;
	}

}
