package ui;

import business.core.DailySchedule;

/**
 * This interface determines what operations have to be implemented for the SchedulePanel to work properly.
 * 
 * @see SchedulePanel
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface ScheduleInterface {

	/**
	 * Get the classes scheduled in a day.
	 * 
	 * @param day
	 * 		The wanted day
	 * @return An instance of DailySchedule
	 */
	DailySchedule getDailySchedule(int day);

}
