package persistence.daos;

import java.util.List;

import business.core.Teacher;
import persistence.daoException.DAOException;

/**
 * Interface to determine the services a TeacherDAO implementation has to provide.
 * This DAO is related to the Teacher table.
 * 
 * Only the "Read" operations of the CRUD are needed in this application.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface TeacherDao {

	/**
	 * Get all teachers.
	 * 
	 * @return a list that contains all the teachers in database
	 * @throws DAOException
	 */
	List<Teacher> find() throws DAOException;
	
	/**
	 * Get a list that contains the working shifts of a teacher
	 * 
	 * @param teacherId
	 * 		The id of the teacher to get shifts
	 * @return A list of integers that contains the working shifts of a teacher.
	 * @throws DAOException
	 */
	List<Integer> findTeacherShifts(int teacherId) throws DAOException;
	
}
