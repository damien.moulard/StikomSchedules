package persistence;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import persistence.daoException.DAOConfigurationException;
import persistence.daos.CourseClassDao;
import persistence.daos.CourseClassDaoImpl;
import persistence.daos.CourseDao;
import persistence.daos.CourseDaoImpl;
import persistence.daos.ScheduleDao;
import persistence.daos.ScheduleDaoImpl;
import persistence.daos.TeacherDao;
import persistence.daos.TeacherDaoImpl;

/**
 * Implementation of the DAO pattern. 
 * This factory is in charge of providing a database connection and creating the different DAOs necessary to communicate with the database.
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class DAOFactory {
 	
	private static DAOFactory instance;

	/**
	 * Path to the properties file necessary to connect with the database
	 */
	private static final String PROPERTIES_FILE = "persistence/dao.properties"; 
	
	/**
	 * Url of the database
	 */
	private static final String PROPERTY_URL = "url";
	
	/**
	 * Driver of the DBMS used
	 */
	private static final String PROPERTY_DRIVER = "driver"; 
	
	/**
	 * User name to connect to the database
	 */
	private static final String PROPERTY_USER = "user"; 
	
	/**
	 * Password to connect to the database
	 */
	private static final String PROPERTY_PASSWORD = "password";
	
	private String url;
	private String user;
	private String password;
	
	/**
	 * DAOFactory constructor. 
	 * 
	 * The factory is a singleton, therefore the constructor is private.
	 * 
	 * @param url
	 * @param username
	 * @param password
	 */
	private DAOFactory( String url, String username, String password ) {
        this.url = url;
        this.user = username;
        this.password = password;
    }

	/**
	 * Create and return an instance of DAOFactory.
	 * 
	 * Load the database connection information from the properties file and load the driver.
	 * The factory can only be created if the properties file is accessible and the driver is present.
	 * 
	 * See the ClassLoader doc for information about the properties file loading.
	 * 
	 * @return an instance of DAOFactory
	 * @throws DAOConfigurationException
	 * 
	 * @see ClassLoader
	 * @see Properties
	 */
	public static DAOFactory getInstance() throws DAOConfigurationException {
		if(instance == null) {
			Properties properties = new Properties();
			String url;
			String driver;
			String user;
			String password;
			
			//Find properties file
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream propertiesFile = classLoader.getResourceAsStream(PROPERTIES_FILE);
			
			if (propertiesFile == null) {
				throw new DAOConfigurationException("Properties file " + PROPERTIES_FILE + " can't be found.");
			}
			
			// Load properties from file
			try {
				properties.load(propertiesFile);
				url = properties.getProperty(PROPERTY_URL);
				driver = properties.getProperty(PROPERTY_DRIVER);
				user = properties.getProperty(PROPERTY_USER);
				password = properties.getProperty(PROPERTY_PASSWORD);
			} catch (IOException e) {
				throw new DAOConfigurationException ("Can't open the properties file " + PROPERTIES_FILE, e);
			}
			
			// Open the driver
			try {
			    Class.forName(driver);
			} catch (ClassNotFoundException e) {
				throw new DAOConfigurationException("Can't find the driver in classpath", e);
		    }
			
			// Create the instance
			instance = new DAOFactory(url, user, password);
		}
		
		return instance;
	}
	
	/**
	 * Provide a database connection.
	 * 
	 * @return an instance of Connection, to connect to the database
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}

	/**
	 * Retrieve an implementation of TeacherDao
	 * 
	 * @return an instance of TeacherDaoImpl
	 * 
	 * @see TeacherDao
	 * @see TeacherDaoImpl
	 */
	public TeacherDao getTeacherDao() {
		return new TeacherDaoImpl(this);
	}
	
	/**
	 * Retrieve an implementation of CourseDao
	 * 
	 * @return an instance of CourseDaoImpl
	 * 
	 * @see CourseDao
	 * @see CourseDaoImpl
	 */
	public CourseDao getCourseDao() {
		return new CourseDaoImpl(this);
	}
	
	/**
	 * Retrieve an implementation of CourseClassDao
	 * 
	 * @return an instance of CourseClassDaoImpl
	 * 
	 * @see CourseClassDao
	 * @see CourseClassDaoImpl
	 */
	public CourseClassDao getCourseClassDao() {
		return new CourseClassDaoImpl(this);
	}
	
	/**
	 * Retrieve an implementation of ScheduleDao
	 * 
	 * @return an instance of ScheduleDaoImpl
	 * 
	 * @see ScheduleDao
	 * @see ScheduleDaoImpl
	 */
	public ScheduleDao getScheduleDao() {
		return new ScheduleDaoImpl(this);
	}
}
