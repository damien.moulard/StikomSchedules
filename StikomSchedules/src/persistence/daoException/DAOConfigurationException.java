package persistence.daoException;

/**
 * DAO exception type created to hide specific exceptions that occur when running.
 * This exception type concerns the DAO configuration.
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class DAOConfigurationException extends RuntimeException {

	private static final long serialVersionUID = -7971847551967546292L;

	public DAOConfigurationException(String message) {
        super(message);
    }

    public DAOConfigurationException( String message, Throwable cause ) {
        super(message, cause);
    }

    public DAOConfigurationException(Throwable cause) {
        super(cause);
    }
}