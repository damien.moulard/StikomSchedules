package business.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import business.GeneticAlgorithm;

/**
 * This is the business class that represents a schedule.
 * 
 * Every schedule share some parameters :
 * <ul>
 * 		<li>The number of days per week.</li>
 * 		<li>The number of hours per day.</li>
 * 		<li>The last morning shift slot.</li>
 * </ul>
 * 
 * A schedule is identified by :
 * <ul>
 * 		<li>A list that represents the different time slots available in a week.
 * 			A slot represent 45 minutes of one day, so a 1,5 hours class occupies 2 slots.
 * 			A slot is also a list because several classes can take place at the same time.</li>
 * 		<li>A hash map that match each class of the schedule with the first time slot it occupies.</li>
 * </ul>
 * 
 * A schedule also has a fitness score calculated by the genetic algorithm.
 * 
 * @see CourseClass
 * @see GeneticAlgorithm
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class Schedule {

	public static final int daysPerWeek = 6;	/* Monday -> Saturday */
	public static final int hoursPerDay = 18; 	/* 8-8:45 am -> 8:45-9:30 pm */
	public static final int lastMorningShiftSlot = 9; /* Morning shift ends at the end of this slot */
	public static final int firstAfternoonShiftSlot = 10; /* Afternoon shift starts at the beginning of this slot */
	
	/* Schedule representation */
	private List<List<CourseClass>> slots;
	private HashMap<CourseClass, Integer> classes;
	
	/**
	 * Fitness score of the schedule
	 * @see GeneticAlgorithm#calculateFitness(Schedule)
	 */
	private float fitness;
	
	/**
	 * Schedule constructor.
	 * Simply initializes the slots list and the classes hashmap.
	 */
	public Schedule() {
		//Init
		this.slots = new ArrayList<List<CourseClass>>();
		ArrayList<CourseClass> slot;
		for (int i = 0; i < daysPerWeek*hoursPerDay; i++) {
			slot = new ArrayList<CourseClass>();
			this.slots.add(slot);
		}
		
		this.classes = new HashMap<CourseClass, Integer>();
	}

	/**
	 * ScheduleSlots getter
	 * @return the slots list of the schedule
	 */
	public List<List<CourseClass>> getSlots() {
		return slots;
	}

	/**
	 * ScheduleClasses getter
	 * @return  the classes hash map of the schedule
	 */
	public HashMap<CourseClass, Integer> getClasses() {
		return classes;
	}
	
	/**
	 * ScheduleFitness getter
	 * @return the fitness score of the schedule
	 * @see Schedule#fitness
	 */
	public float getFitness() {
		return fitness;
	}

	/**
	 * ScheduleFitness setter
	 * @param fitness
	 * 		the fitness score to set to the schedule
	 * @see Schedule#fitness
	 */
	public void setFitness(float fitness) {
		this.fitness = fitness;
	}
	
	/**
	 * Generate a random schedule from a list of classes
	 * 
	 * @param classList
	 * 		List of all the classes needed to build the schedule
	 */
	public void generateRandom(List<CourseClass> classList) {		
	    Random rand = new Random();
	    int duration;
	    int day;
	    int time;
	    int position;
	    
	    //For each class
		for (int i = 0; i < classList.size(); i++) {
			//Determine random position
			duration = classList.get(i).getDuration();
		    day = rand.nextInt(daysPerWeek);
		    time = rand.nextInt(hoursPerDay - duration);
			position = day*hoursPerDay + time;
			
			//Fill time slots for each hour of the class
			for (int j = 0; j < duration; j++) {
				this.slots.get(position + j).add(classList.get(i));
			}
			
			//Fill the hash map
			this.classes.put(classList.get(i), position);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public String toString() {
		Iterator<Entry<CourseClass, Integer>> it = classes.entrySet().iterator();
		Map.Entry pair;
		int classDay;
		int classHour;
		int teacherShift;
		
		while (it.hasNext()) {
			pair = it.next();
			
			classDay = ((Integer)pair.getValue() / Schedule.hoursPerDay);
			classHour = (Integer)pair.getValue() - (classDay * Schedule.hoursPerDay);
			teacherShift = ((CourseClass) pair.getKey()).getTeacher().getWorkingShifts().get(classDay);
			
			System.out.println("Class " + ((CourseClass) pair.getKey()).getId());
			System.out.println("	Occures at day " + classDay + ", at time slot" + classHour);
			System.out.println("	It's duration : " + ((CourseClass) pair.getKey()).getDuration());
			System.out.println("	Teacher :  " + ((CourseClass) pair.getKey()).getTeacher().getName());
			System.out.println("	Teacher's shift :  " + teacherShift);
		}
		System.out.println("fitness: " + fitness);
		return null;
	}
}
