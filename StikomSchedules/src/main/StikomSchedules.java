package main;

import java.awt.EventQueue;
import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import business.GeneticAlgorithm;
import business.MainWindowBusiness;
import persistence.DAOFactory;
import persistence.daoException.DAOConfigurationException;
import ui.MainWindowFrame;

/**
 * Welcome to the Stikom Schedules application!
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class StikomSchedules {

	/**
	 * Launch the application.
	 * 
	 * @param args
	 * 		Command-line arguments. 
	 */
	public static void main(String[] args) {		
		// Setting the java generated UIs to english
		JComponent.setDefaultLocale(Locale.ENGLISH);

		// Initialize the factory for database interactions
		try {
			DAOFactory.getInstance();
		} catch(DAOConfigurationException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Error with database initialization", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
				
		// Initialize the genetic algorithm
		GeneticAlgorithm algo = new GeneticAlgorithm();
				
		// Display the main window
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindowFrame frame = new MainWindowFrame(new MainWindowBusiness(), algo);
					frame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					System.exit(0);
				}
			}
		});
		/**/
		
		
		/* sandbox */		
		/*ScheduleDao scheduleDao = DAOFactory.getInstance().getScheduleDao();
		
		// Get Schedule table content
		Schedule sc = scheduleDao.find();
		
		// If table not empty
		if (sc != null) {
			// Clear table if you want to save a new one
			scheduleDao.clear();
			
			//sc.toString();
		}
		
		// Run genetic algorithm
		algo.run();
		
		// Get Result
		Schedule sc1 = algo.getFinalBestChromosome();

		// Print result
		System.out.println("\n**************\nResult:");
		//sc1.toString();
		
		// Save the schedule
		scheduleDao.save(sc1);
		/**/
	}

}
