package persistence.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import persistence.DAOFactory;
import persistence.DAOUtility;
import persistence.daoException.DAOException;
import business.core.Course;
import business.core.CourseClass;
import business.core.Teacher;

/**
 * The CourseClassDAO implementation class.
 * 
 * @author Damien Moulard
 * @version 1.0
 * 
 * @see CourseClassDao
 */
public class CourseClassDaoImpl implements CourseClassDao {
	
	private DAOFactory daoFactory;
	private static final String SQL_SELECT_ALL = "SELECT * FROM CourseClass";

	/**
	 * CourseClassDaoImpl constructor
	 * 
	 * @param factory
	 * 		The DAOFactory that provides the database connection
	 * 
	 * @see DAOFactory
	 */
	public CourseClassDaoImpl (DAOFactory factory) {
		this.daoFactory = factory;
	}
	
	/**
	 * Match the result of a query with a CourseClass business class. 
	 * 
	 * @param resultSet
	 * 		The result of the query
	 * @param teachers
	 * 		List of teachers to retrieve the class teacher
	 * @param courses
	 * 		List of courses to retrieve the class teacher
	 * @return an instance of CourseClass that matches the query result. 
	 * @throws SQLException
	 * 
	 * @see CourseClass
	 */
	private static CourseClass match (ResultSet resultSet, ArrayList<Teacher> teachers, ArrayList<Course> courses) throws SQLException {
		Teacher teacher = null;
		Course course = null;
		
		// Find matching teacher
		for (int i = 0; i < teachers.size(); i++) {
			if (teachers.get(i).getId() == resultSet.getInt("classTeacherId")) {
				teacher = teachers.get(i);
			}
		}
		
		// Find matching course
		for (int i = 0; i < courses.size(); i++) {
			if (courses.get(i).getId() == resultSet.getInt("classCourseId")) {
				course = courses.get(i);
			}
		}
		
		CourseClass courseClass = new CourseClass(resultSet.getInt("classId"), resultSet.getInt("classDuration"), teacher, course);
		return courseClass;
	}
	
	/**
	 * @see DAOUtility
	 * @see CourseClassDaoImpl#match(ResultSet, ArrayList, ArrayList)
	 * @see TeacherDaoImpl
	 * @see CourseDaoImpl
	 * @see CourseClass 
	 */
	@Override
	public List<CourseClass> find() throws DAOException {
		Connection connection = null;
		PreparedStatement preparedQuery = null;
		ResultSet resultSet = null;
		CourseClass courseClass;
		ArrayList<CourseClass> courseClassList = new ArrayList<CourseClass>();
		
		try {
			// Get connection from the factory
			connection = (Connection) daoFactory.getConnection();
			
			// Prepare the query
			preparedQuery = DAOUtility.initPreparedQuery(connection, SQL_SELECT_ALL, false);
			
			// Retrieve results
			resultSet = preparedQuery.executeQuery();
			
			
			// Get all teachers
			TeacherDao teacherDao = daoFactory.getTeacherDao();
			ArrayList<Teacher> teachers = (ArrayList<Teacher>) teacherDao.find();
			
			// Get all courses
			CourseDao courseDao = daoFactory.getCourseDao();
			ArrayList<Course> courses = (ArrayList<Course>) courseDao.find();
						
			
			while (resultSet.next()) {
				// Match result with a CourseClass class before adding it to the list
				courseClass = match(resultSet, teachers, courses);
				courseClassList.add(courseClass);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			// Close connections
			DAOUtility.close(resultSet, preparedQuery, connection);
		}
		
		return courseClassList;
	}
}