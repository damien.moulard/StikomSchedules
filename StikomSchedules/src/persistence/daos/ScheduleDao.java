package persistence.daos;

import persistence.daoException.DAOException;
import business.core.DailySchedule;
import business.core.Schedule;

/**
 * Interface to determine the services a ScheduleDAO implementation has to provide.
 * This DAO is related to the Schedule table.
 * 
 * Contains "Create","Read" and "Delete" CRUD operations.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface ScheduleDao {

	/**
	 * Save a schedule.
	 * The Schedule table can only contain one schedule, it must therefore be cleared before saving a new schedule.
	 * 
	 * @param sc
	 * 		The schedule to save into the database
	 * @throws DAOException 
	 */
	void save(Schedule sc) throws DAOException;
	
	/**
	 * Retrieve the schedule stored in database.
	 * 
	 * @return An instance of Schedule
	 * @throws DAOException
	 */
	Schedule find() throws DAOException;
	
	/**
	 * Get the classes and their slot scheduled at the given day
	 * 
	 * @param dayId
	 * 		The id of the selected day
	 * @return An instance of DalySchedule
	 * @throws DAOException
	 */
	DailySchedule findByDay(int dayId) throws DAOException;
	
	/**
	 * Clear the Schedule table
	 * 
	 * @throws DAOException
	 */
	void clear() throws DAOException;
}
