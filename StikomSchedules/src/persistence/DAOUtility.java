package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

/**
 * DAOUtility contains methods shared by all the DAOs.
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class DAOUtility {
 
	/**
	 * DAOUtility constructor
	 */
	private DAOUtility() {
		super();
	}

	/**
	 * Init a prepared statement based on a connection, with the given SQL query and the given query parameters.
	 * 
	 * @param connection
	 * 		For the database connection
	 * @param query
	 * 		SQL query to prepare
	 * @param returnGeneratedKeys
	 * 		If true, the prepared statement can return the new line(s) that has been inserted (useful to retrieve auto-incremented keys).
	 * 		Should always be "false" for reading operations. 
	 * @param objects
	 * 		A sequence of parameters for the query. "..." indicates it can be of any size (even 0), and "Object" that each can be of any type.
	 * 
	 * @return the PreparedStatement that matches all the parameters
	 * @throws SQLException
	 * 
	 * @see PreparedStatement
	 */
	public static PreparedStatement initPreparedQuery (Connection connection, String query, boolean returnGeneratedKeys, Object... objects) throws SQLException {
		PreparedStatement preparedQuery = (PreparedStatement) connection.prepareStatement(query, returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
		// Add the parameters given in parameter to the query
		for (int i = 0; i < objects.length; i++) {
			preparedQuery.setObject(i + 1, objects[i]);
		}
		return preparedQuery;
	}
	
	/**
	 * Close a ResultSet instance
	 * @param resultSet
	 * 		The instance to close
	 */
	public static void close (ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				System.out.println("The closure of the ResultSet has failed : " + e.getMessage());
			}
		}
	}
	
	/**
	 * Close a Statement instance
	 * @param statement
	 * 		The instance to close
	 */
	public static void close (Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				System.out.println("The closure of the Statement has failed : " + e.getMessage());
			}
		}
	}
	
	/**
	 * Close a Connection instance
	 * @param connection
	 * 		The instance to close
	 */
	public static void close (Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("The closure of the connection has failed : " + e.getMessage());
			}
		}
	}
	
	/**
	 * Close method for queries that don"t use a ResultSet
	 * 
	 * @param statement
	 * 		Statement instance to close
	 * @param connection
	 * 		Connection instance to close
	 * 
	 * @see DAOUtility#close(Statement)
	 * @see DAOUtility#close(Connection)
	 */
	public static void close (Statement statement, Connection connection) {
		close(statement);
		close(connection);
	}
	
	/**
	 * Close method for queries that use a ResultSet
	 * 
	 * @param resultSet
	 * 		ResultSet instance to close
	 * @param statement
	 * 		Statement instance to close
	 * @param connection
	 * 		Connection instance to close
	 * 
	 * @see DAOUtility#close(ResultSet)
	 * @see DAOUtility#close(Statement)
	 * @see DAOUtility#close(Connection)
	 */
	public static void close (ResultSet resultSet, Statement statement, Connection connection) {
		close(resultSet);
		close(statement);
		close(connection);
	}
}
