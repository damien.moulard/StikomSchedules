package business.core;

import java.util.List;

/**
 * This is the business class that represents a teacher.
 * 
 * A teacher is identified by an unique id and a name.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class Teacher {

	private int id;
	private String name;
	
	/**
	 * Represents the working shifts of a teacher.
	 * It's size is the number of days in the week.
	 * For each day, 1 is a morning shift, 2 is an afternoon shift, and 0 means he doesn't work that day.
	 */
	private List<Integer> workingShifts;
	
	/**
	 * Teacher constructor
	 * 
	 * @param id
	 * 		Teacher's id
	 * @param name
	 * 		Teacher's name
	 * @param shifts
	 * 		Teacher's working shifts
	 */
	public Teacher(int id, String name, List<Integer> shifts) {
		this.id = id;
		this.name = name;
		this.workingShifts = shifts;
	}

	/**
	 * TeacherId getter
	 * @return the teacher's id
	 */
	public int getId() {
		return id;
	}

	/**
	 * TeacherName getter
	 * @return the teacher's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * TeacherWorkingShifts getter
	 * @return the teacher's working shifts as a list of integers
	 * 
	 * @see Teacher#workingShifts
	 */
	public List<Integer> getWorkingShifts() {
		return workingShifts;
	}
}
