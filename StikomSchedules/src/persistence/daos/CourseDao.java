package persistence.daos;

import java.util.List;

import business.core.Course;
import persistence.daoException.DAOException;

/**
 * Interface to determine the services a CourseDAO implementation has to provide.
 * This DAO is related to the Course table.
 * 
 * Only the "Read" operations of the CRUD are needed in this application.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface CourseDao {

	/**
	 * Get all courses.
	 * 
	 * @return a list that contains all the courses in database
	 * @throws DAOException
	 */
	List<Course> find() throws DAOException;
	
}
