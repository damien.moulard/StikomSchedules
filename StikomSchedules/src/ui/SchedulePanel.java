package ui;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import business.core.DailySchedule;
import business.core.Schedule;

import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

/**
 * This is the panel that displays a schedule.
 * It contains an interface that defines the required operations for the panel to be functional.
 * 
 * It uses the DailySchedule class to display, for each day, the classes scheduled at this day.
 * 
 * @see Schedule
 * @see ScheduleInterface
 * @see DailySchedule
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class SchedulePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5325687074413102946L;
	
	private ScheduleInterface business;
	private Schedule schedule;
	
	/**
	 * Create the panel.
	 *
	 * @param scheduleBusiness
	 * 		The business class for this panel, implementation of the ScheduleInterface.
	 * @param sc
	 * 		The schedule to display (only used to display fitness score)
	 */
	public SchedulePanel(ScheduleInterface scheduleBusiness, Schedule sc) {
		this.business = scheduleBusiness;
		this.schedule = sc;
		DailySchedule dailySchedule;
		
		setLayout(new MigLayout("", "[grow]", "[][][][][]"));
				
		Object datas[][];
		JLabel dayLabel;
		JTable dayTable;
		int layoutGridCounter = 0;
		
		// Display schedule's fitness score
		JLabel fitnessLabel = new JLabel("Fitness score: " + schedule.getFitness());
		add(fitnessLabel, "cell 0 " + layoutGridCounter);
		layoutGridCounter++;
		
		// For each day
		for (int i = 1; i <= Schedule.daysPerWeek; i++) {
			// Get database content for this day
			dailySchedule = business.getDailySchedule(i);
			
			// If classes are planned this day
			if (dailySchedule != null) {
				// Create a 2 dimensions array for the JTable model
				datas = new Object[dailySchedule.getClasses().size()][4];
				for (int j = 0; j < dailySchedule.getClasses().size(); j++) {
					datas[j][0] = dailySchedule.getClassesSlot().get(j);
					datas[j][1] = dailySchedule.getClasses().get(j).getCourse().getName();
					datas[j][2] = dailySchedule.getClasses().get(j).getTeacher().getName();
					datas[j][3] = dailySchedule.getClasses().get(j).getDuration() + " class hour(s)";
				}

				// Create a label for the day
				dayLabel = new JLabel(dailySchedule.getDay());
				add(dayLabel, "cell 0 " + layoutGridCounter);
				layoutGridCounter++;
				
				// Create a table for this day schedule
				dayTable = new JTable();
				dayTable.setModel(new DefaultTableModel(datas, new String[] { "Class starts at", "Course", "Teacher", "Duration"}));
				add(dayTable, "cell 0 " + layoutGridCounter + ",grow");
				//add((new JScrollPane(dayTable)), "cell 0 " + layoutGridCounter + ",grow");
				layoutGridCounter++;
			}
			
		}
	}

}
