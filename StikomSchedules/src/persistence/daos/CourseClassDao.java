package persistence.daos;

import java.util.List;

import persistence.daoException.DAOException;
import business.core.CourseClass;

/**
 * Interface to determine the services a CourseClassDAO implementation has to provide.
 * This DAO is related to the CourseClass table.
 * 
 * Only the "Read" operations of the CRUD are needed in this application.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface CourseClassDao {

	/**
	 * Get all classes.
	 * 
	 * @return a list that contains all the classes in database
	 * @throws DAOException
	 */
	List<CourseClass> find() throws DAOException;
	
}
