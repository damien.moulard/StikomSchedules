package business;

import persistence.DAOFactory;
import persistence.daos.ScheduleDao;
import business.core.DailySchedule;
import ui.ScheduleInterface;

/**
 * Implementation of the ScheduleInterface.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class SchedulePanelBusiness implements ScheduleInterface {

	private DAOFactory daoFactory;

	/**
	 * SchedulePanelBusiness constructor.
	 * Retrieves the DAOFactory instance for database interactions.
	 * 
	 * @see DAOFactory
	 */
	public SchedulePanelBusiness() {
		super();
		daoFactory = DAOFactory.getInstance();
	}
	
	/**
	 * @see DailySchedule
	 * @see ScheduleDao
	 */
	@Override
	public DailySchedule getDailySchedule(int day) {
		ScheduleDao scheduleDao = daoFactory.getScheduleDao();
		return scheduleDao.findByDay(day);
	}

}
