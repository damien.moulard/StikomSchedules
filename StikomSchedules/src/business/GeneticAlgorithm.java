package business;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import business.core.CourseClass;
import business.core.Schedule;
import persistence.DAOFactory;
import persistence.daos.CourseClassDao;

/**
 * This is the business class for running the genetic algorithm.
 * 
 * This class has several attributes corresponding to the genetic algorithm parameters.
 *  
 * The algorithm has a list of schedules that represents the population, 
 * and another list that contains the best chromosomes of the population.
 * 
 * A DAO pattern has been implemented to interact with the persistence layer.
 * Therefore, the class also has a DAOFactory, which will create the different DAOs.
 * 
 * @see Schedule
 * @see DAOFactory
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class GeneticAlgorithm {
	
	/**
	 * The DAOFactory for interacting with the persistence layer.
	 * @see DAOFactory
	 */
	private DAOFactory daoFactory;

	
	/* Algorithm parameters */
	
	/** 
	 * The number of generations, i.e. how many times loops the algorithm will perform. 
	 */
	private final int nbOfGenerations;
	
	/**
	 * Number of chromosomes.
	 */
	private final int populationSize;
	
	/**
	 * How many chromosomes in the best group.
	 */
	private final int bestGroupSize;
	
	/**
	 * Number of criteria to take into account to calculate the fitness score.
	 */
	private final int nbOfSoftConstraints;
	
	/**
	 * Number of offsprings to try to create in every generation.
	 */
	private final int replacementsByGeneration;
	
	/**
	 * Number of crossover points of each chromosome.
	 */
	private final int nbOfCrossoverPoints;
	
	/**
	 * In how many parts we separate a chromosome for the mutation.
	 */
	private final int mutationSize;
	
	/**
	 * Probability for a crossover to occur.
	 */
	private final int crossoverProbability;
	
	/**
	 * Probability for a mutation to occur.
	 */
	private final int mutationProbability;
	
	
	/**
	 * List of schedules to represent the population.
	 * @see Schedule
	 */
	private List<Schedule> chromosomes;
	
	/**
	 * List of integers to represent the indices of the best chromosomes
	 */
	private List<Integer> bestChromosomes;
	
	/**
	 * The schedule produced by the algorithm
	 */
	Schedule finalBestChromosome;


	/**
	 * GeneticAlgorithm constructor.
	 * Initialize and set the genetic algorithm parameters
	 */
	public GeneticAlgorithm() {		
		this.nbOfGenerations = 200;			// Should be over 10, no need to go over 500.
		this.populationSize = 100;			// Between 100 and 150 should be enough.
		this.replacementsByGeneration = 8;	// At least 1. Better results around 10.
		this.bestGroupSize = 15;			// Between 1 and populationSize-1. Better results between 1/10 and 2/10 of the population.
		this.nbOfSoftConstraints = 1;		// IMPORTANT: keep this parameter up to date! But don't EVER set it to 0 (bad stuff happen), at least 1.
		this.nbOfCrossoverPoints = 2;		// Maximum is the number of classes. 2 or 3 should be enough.
		this.mutationSize = 2;				// Maximum is the number of classes. 2 or 3 should be enough.
		this.crossoverProbability = 80;		// Between 0 and 100. High probability for crossover.
		this.mutationProbability = 20;		// Between 0 and 100. Probability for mutation should be lower.

		this.chromosomes = new ArrayList<Schedule>(populationSize);
		this.bestChromosomes = new ArrayList<Integer>(bestGroupSize);
		
		this.daoFactory = DAOFactory.getInstance();
	}

	/**
	 * Run the genetic algorithm.
	 * 
	 * The first step is the generation of a random population of chromosomes from the list of all classes.
	 * A randomly generated chromosome is accepted in the population only if it doesn't break a hard constraint.
	 * Then, for each generation, the following steps are repeated as many times as the replacementsByGeneration variable value :
	 * <ul>
	 * 		<li>First comes the crossover step, from two randomly chosen chromosomes among the best group.</li>
	 * 		<li>If the crossover occurred, and if offspring doesn't break any hard constraint,
	 * 			replace a random chromosome in the population (that is not in the best group) by the offspring
	 * 			and try to add it to the best group.</li>
	 * 		<li>Then comes the mutation step on a randomly chosen chromosome among the best group.</li>
	 * 		<li>If the mutation occurred, if mutated chromosome doesn't break any hard constraint and if it's better than the old one,
	 * 			replace the old chromosome by its mutation and try to add it to the best group</li>
	 * </ul>
	 * 
	 * The loop stops if a chromosome fitness score has reached 1. 
	 * 
	 * 
	 * @see GeneticAlgorithm#replacementsByGeneration
	 * 
	 * @see Schedule#generateRandom(List)
	 * @see GeneticAlgorithm#crossover(Schedule, Schedule)
	 * @see GeneticAlgorithm#mutation(Schedule)
	 * @see GeneticAlgorithm#isInBest(int)
	 * @see GeneticAlgorithm#addToBest(int)
	 * @see GeneticAlgorithm#getBest()
	 */
	@SuppressWarnings("unchecked")
	public void run() {
		// Init or reset
		chromosomes = new ArrayList<Schedule>(populationSize);
		bestChromosomes = new ArrayList<Integer>(bestGroupSize);
		
		// Get all the classes from database
		CourseClassDao classDao = daoFactory.getCourseClassDao();
		ArrayList<CourseClass> classList = (ArrayList<CourseClass>) classDao.find();

		Schedule sc;
		int i = 0;
		
		/* Initialize the population with randomly built chromosomes */
		while (i < populationSize) {
			// Get new chromosome
			sc = new Schedule();
			sc.generateRandom(classList);
			
			//If chromosome not viable, generate a new one
			if (viableChromosome(sc)) {
				// Calculate new chromosome fitness
				calculateFitness(sc);
				
				// Add to the list
				chromosomes.add(sc);
				
				// Try to add to best group
				addToBest(i);
				
				i++;
			}
		}

		ArrayList<Integer> bestChromosomesCopy;
		Schedule parent1; 
		Schedule parent2; 
		Schedule offspring;
		Schedule bestOfGeneration;
		Random rand = new Random();
		int chromosomeIndex;
		float chromosomeFitness;
		
		int currentGeneration = 0;
		boolean bestReached = false;
		
		while (currentGeneration < nbOfGenerations && !bestReached) {
			
			for (int j = 0; j < replacementsByGeneration; j++) {
				/* Crossover */
				
				// Select first random parent
				chromosomeIndex = rand.nextInt(bestChromosomes.size());
				parent1 = chromosomes.get(bestChromosomes.get(chromosomeIndex));
				
				// Select second random parent
				if (bestChromosomes.size() == 1) {
					// If best group only had one element, pick a random chromosome in population (very low chance to have two identical parents)
					parent2 = chromosomes.get(rand.nextInt(chromosomes.size()));
				} else {
					// Otherwise remove the first parent from best group before, so we don't have two identical parents
					bestChromosomesCopy = (ArrayList<Integer>) ((ArrayList<Integer>) bestChromosomes).clone();
					bestChromosomesCopy.remove(chromosomeIndex);
					chromosomeIndex = rand.nextInt(bestChromosomesCopy.size());
					parent2 = chromosomes.get(bestChromosomes.get(chromosomeIndex));
				}
				
				// Try to produce offspring
				offspring = crossover(parent1, parent2);
				
				// If crossover occurred and offspring is viable
				if (offspring != null && viableChromosome(offspring)) {
					// Select random chromosome in population that isn't in the best group for replacement
					chromosomeIndex = rand.nextInt(populationSize);
					while (isInBest(chromosomeIndex)) {
						chromosomeIndex = rand.nextInt(populationSize);
					}
					
					// Replacement
					chromosomes.remove(chromosomeIndex);
					chromosomes.add(chromosomeIndex, offspring);
					
					// Try to add to best group
					addToBest(chromosomeIndex);
				}
				
				
				/* Mutation */
				
				// Select random chromosome in the best group to mutate
				chromosomeIndex = rand.nextInt(bestChromosomes.size());
				parent1 = chromosomes.get(bestChromosomes.get(chromosomeIndex));
				chromosomeFitness = parent1.getFitness();
				
				// Try to mutate chromosome
				if (mutation(parent1)) {
					
					// If mutation occurred, if the mutation is viable and if it's better
					if (viableChromosome(parent1) && parent1.getFitness() > chromosomeFitness) {
						
						// Replace old chromosome by its mutation
						if (isInBest(chromosomeIndex)) {
							bestChromosomes.remove((Object)chromosomeIndex);
						}
						chromosomes.remove(chromosomeIndex);
						chromosomes.add(chromosomeIndex, parent1);
						
						// Try to add to best group
						addToBest(chromosomeIndex);
					}
				}
			}
			
			// If a chromosome with a fitness score of 1 has been reach, no need to proceed
			bestOfGeneration = getBest();
			if (bestOfGeneration.getFitness() == 1) {
				bestReached = true;
			}
			
			currentGeneration++;
		}
		
		System.out.println(currentGeneration);
		
		// Store the final best chromosome
		finalBestChromosome = getBest();
	}

	/**
	 * Crossover operation of the genetic algorithm on the given chromosomes: produces offspring from two parents.
	 * 
	 * The crossover points are randomly chosen.
	 * The hashmaps of both parents are browsed along.
	 * The classes of a parent are added one by one.
	 * When a crossover point is reached, the classes of the other parent are added one by one.
	 * The parents are switched in this way every time a crossover point is reached.
	 * 
	 * When the crossover is over, the fitness score of the offspring is calculated.
	 * 
	 * @param parent1
	 * 		An instance of Schedule
	 * @param parent2
	 * 		An instance of Schedule
	 * @return a schedule, result of the crossover operation, if the crossover occurred. Return null if it didn't.
	 * 
	 * @see GeneticAlgorithm#nbOfCrossoverPoints
	 * @see GeneticAlgorithm#crossoverProbability
	 * @see GeneticAlgorithm#calculateFitness(Schedule)
	 */
	@SuppressWarnings("rawtypes")
	private Schedule crossover(Schedule parent1, Schedule parent2) {
		Random rand = new Random();

		// Check the probability of a crossover to occur
		if (rand.nextInt(101) > crossoverProbability) {
			return null;
		}

		int nbOfClasses = parent1.getClasses().size();

		// Randomly determine the crossover points
		boolean[] crossoverPoints = new boolean[nbOfClasses];
		for (int i = 0; i < parent1.getClasses().size(); i++) {
			crossoverPoints[i] = false;
		}

		int cp;
		for (int i = 0; i < nbOfCrossoverPoints; i++) {
			cp = rand.nextInt(nbOfClasses);

			// Check if randomly chosen point is not already a crossover point
			while (crossoverPoints[cp])
				cp = rand.nextInt(nbOfClasses);

			crossoverPoints[cp] = true;
		}

		// Create the offspring
		Schedule theNewGuy = new Schedule();

		// Get an iterator of the 2 parents hash map
		Iterator<Entry<CourseClass, Integer>> it1 = parent1.getClasses().entrySet().iterator();
		Iterator<Entry<CourseClass, Integer>> it2 = parent2.getClasses().entrySet().iterator();

		// Choose a parent to start from (true will be parent 1 and false will be parent 2). Let's randomize this!
		boolean currentParent = rand.nextBoolean();

		Map.Entry pair;
		CourseClass parentClass;
		int parentClassPosition;
		for (int i = 0; i < nbOfClasses; i++) {
			if (currentParent) {
				pair = it1.next();
				parentClass = (CourseClass) pair.getKey();
				parentClassPosition = (Integer) pair.getValue();

				// Insert class from first parent into the new chromosome's hash map
				theNewGuy.getClasses().put(parentClass, parentClassPosition);

				// Then copy the first parent time slots
				for (int j = 0; j < parentClass.getDuration(); j++) {
					theNewGuy.getSlots().get(parentClassPosition + j)
							.add(parentClass);
				}

				// Both iterators move along
				it2.next();
			} else {
				// All the same with the other parent
				pair = it2.next();
				parentClass = (CourseClass) pair.getKey();
				parentClassPosition = (Integer) pair.getValue();

				theNewGuy.getClasses().put(parentClass, parentClassPosition);

				for (int j = 0; j < parentClass.getDuration(); j++) {
					theNewGuy.getSlots().get(parentClassPosition + j)
							.add(parentClass);
				}

				it1.next();
			}

			// If we are at a crossover point, switch parent
			if (crossoverPoints[i]) {
				currentParent = !currentParent;
			}
		}
		
		//Calculate the fitness score of the offspring
		calculateFitness(theNewGuy);
		return theNewGuy;
	}

	/**
	 * Mutation operation of the genetic algorithm on the given chromosome:
	 * Change the position of some classes in the schedule.
	 * 
	 * Randomly choose a class, remove it from its current position and put it in its new, randomly chosen position.
	 * Do this as many times as mutationSize value.
	 * 
	 * Re-calculate the fitness score after mutation.
	 * 
	 * @param sc
	 * 		The instance of Schedule to perform the mutation on.
	 * @return True if mutation occured, false otherwise.
	 * 
	 * @see GeneticAlgorithm#mutationSize
	 * @see GeneticAlgorithm#mutationProbability
	 * @see GeneticAlgorithm#calculateFitness(Schedule)
	 */
	@SuppressWarnings("rawtypes")
	private boolean mutation(Schedule sc) {
		Random rand = new Random();

		// Check the probability of a mutation to occur
		if (rand.nextInt(101) > mutationProbability) {
			return false;
		}
		
		int nbOfClasses = sc.getClasses().size();
		int randomPosition;
		Iterator<Entry<CourseClass, Integer>> it;
		Map.Entry pair = null;
		CourseClass currentClass;
		int currentClassPosition;
		int duration;
		int day;
		int time;
		int newPosition;
		
		for (int i = 0; i < mutationSize; i++) {
			// Select a random class to move
			randomPosition = rand.nextInt(nbOfClasses);
			it = sc.getClasses().entrySet().iterator();
			while (randomPosition >= 0) {
				pair = it.next();
				randomPosition--;
			}
			
			currentClass = (CourseClass) pair.getKey();
			currentClassPosition = (Integer) pair.getValue();
			
			// Determine a random new position
			duration = currentClass.getDuration();
		    day = rand.nextInt(Schedule.daysPerWeek);
		    time = rand.nextInt(Schedule.hoursPerDay - duration);
			newPosition = day*Schedule.hoursPerDay + time;
			
			for (int j = 0; j < duration; j++) {
				// Remove class from current timeslots
				sc.getSlots().get(currentClassPosition + j).remove(currentClass);
				
				// Add class to new timeslots
				sc.getSlots().get(newPosition + j).add(currentClass);
			}
			
			// Change entry in the class table to point to the new position
			sc.getClasses().put(currentClass, newPosition);
		}
	
		// Re-calculate the fitness score
		calculateFitness(sc);
			
		return true;
	}
	
	/**
	 * Method to check hard constraints on a chromosome.
	 * 
	 * Each class of the chromosome is checked. 
	 * If one break a hard constraint, the chromosome isn't viable.
	 * 
	 * @param sc
	 * 		The schedule to check hard constraints on.
	 * @return True if the chromosome complies with all hard constraints. Otherwise, returns false.
	 * 
	 * @see GeneticAlgorithm#nbOfHardConstraints
	 */
	@SuppressWarnings("rawtypes")
	private boolean viableChromosome (Schedule sc) {
		boolean chromosomeIsViable = true;
		
		// Get an iterator of the schedule classes
		Iterator<Entry<CourseClass, Integer>> it = sc.getClasses().entrySet().iterator();
			
		Map.Entry pair;
		CourseClass currentClass;
		int currentClassPosition;
		ArrayList<CourseClass> classes;
		
		boolean teacherHasOneClass;
		boolean workingDaysRespected;
		
		int i;
		
		while (it.hasNext() && chromosomeIsViable) {
			pair = it.next();
			currentClass = (CourseClass) pair.getKey();
			currentClassPosition = (Integer) pair.getValue();
			
			/* 
			 * Hard constraint n�1
			 * Check if a teacher doesn't have another class at the same time
			 */
			teacherHasOneClass = true;
			i = 0;
			while (i < currentClass.getDuration() && teacherHasOneClass) {
				// Get the classes at this time slot
				classes = (ArrayList<CourseClass>) sc.getSlots().get(currentClassPosition + i);
				for (int j = 0; j < classes.size(); j++) {
					// Check for teacher overlaps
					if (!classes.get(j).equals(currentClass) && classes.get(j).getTeacher().equals(currentClass.getTeacher())) {
						teacherHasOneClass = false;
					}
				}
				i++;
			}
			
			if (!teacherHasOneClass) 
				chromosomeIsViable = false;
			
			
			/*
			 * Hard constraint n�2
			 * Working days: Check if a teacher doesn't have a class during a day he doesn't work
			 */
			workingDaysRespected = true;
			i = 0;
			int classDay;
			int teacherWorkingDay;
			
			classDay = (currentClassPosition / Schedule.hoursPerDay);
			teacherWorkingDay = currentClass.getTeacher().getWorkingShifts().get(classDay);
			
			while (i < currentClass.getDuration() && workingDaysRespected) {
				// If the teacher doesn't work that day
				if (teacherWorkingDay == 0) {
					workingDaysRespected = false;
				}
				i++;
			}
			
			if (!workingDaysRespected) {
				chromosomeIsViable = false;
			}
		}
		
		return chromosomeIsViable;
	}

	/**
	 * Method to check soft constraints on a chromosome.
	 * Calculate the fitness score of the given chromosome.
	 * 
	 * The chromosome has a global score, and each class of the chromosome is checked.
	 * Every time a class match a criteria, add 1 to the score.
	 * The fitness is calculated from this global core, after every class has been checked.
	 * 
	 * @param sc
	 * 		The schedule to calculate the fitness score.
	 * 
	 * @see GeneticAlgorithm#nbOfSoftConstraints
	 */
	@SuppressWarnings("rawtypes")
	public void calculateFitness(Schedule sc) {
		int score = 0;
		
		Map.Entry pair;
		CourseClass currentClass;
		int currentClassPosition;
		
		//boolean oneClass;
		boolean shiftRespected;
		int classDay;
		int classHour;
		int teacherShift;
		
		int i;
		
		// Get an iterator of the schedule classes
		Iterator<Entry<CourseClass, Integer>> it = sc.getClasses().entrySet().iterator();
		
		//For each class
		while (it.hasNext()) {
			pair = it.next();
			currentClass = (CourseClass) pair.getKey();
			currentClassPosition = (Integer) pair.getValue();
			
			/*
			 * Soft Constraint n�1
			 * Working shifts : Check if a class complies with the teacher working shift
			 */
			shiftRespected = true;
			i = 0;
			classDay = (currentClassPosition / Schedule.hoursPerDay);
			classHour = currentClassPosition - (classDay * Schedule.hoursPerDay);
			teacherShift = currentClass.getTeacher().getWorkingShifts().get(classDay);
			
			while (i < currentClass.getDuration() && shiftRespected) {
				if (teacherShift == 1 && classHour + i > Schedule.lastMorningShiftSlot) {
					// If the teacher has a morning shift this day and the class occupies an afternoon shift slot
					shiftRespected = false;
				} else if (teacherShift == 2 && classHour + i < Schedule.firstAfternoonShiftSlot){
					// If the teacher has an afternoon shift this day and the class occupies a morning shift slot
					shiftRespected = false;
				}
				i++;
			}
			
			if (shiftRespected)
				score++;
			
			
			/* 
			 * Optional soft constraint 
			 * Check if there isn't another class at the same time
			 * 
			 * This one was implemented to get a cleaner schedule on screen, but useless in practice.
			 * Can be re-implemented if wanted.
			 */
			/*
			oneClass = true;
			i = 0;
			while (i < currentClass.getDuration() && oneClass) {
				if (sc.getSlots().get(currentClassPosition + i).size() > 1) {
					oneClass = false;
				}
				i++;
			}
			
			if (oneClass)
				score++;
			*/
		}

		// nbOfClasses*nbOfSoftConstraints is the maximum score a chromosome can get
		int nbOfClasses = sc.getClasses().size();
		sc.setFitness(((float)score) / (nbOfClasses*nbOfSoftConstraints));
	}

	/**
	 * Try to add a chromosome in the best group.
	 * 
	 * The lower the index in the bestChromosomes list, the higher the fitness score.
	 * 
	 * If best group is empty, simply add the chromosome.
	 * Otherwise, check if the chromosome is good enough (fitness higher than the worst of the bests) and if it's not already in the list.
	 * If conditions passed, add to the bestChromosomes list and move following chromosomes one place further.
	 * If bestGroupSize overstepped, remove last element.
	 * 
	 * @param chromosomeIndex
	 * 		The index of the chromosome to add at the best group.
	 * @return True if chromosome added to best group
	 * @see GeneticAlgorithm#bestChromosomes
	 * @see GeneticAlgorithm#bestGroupSize
	 */
	private boolean addToBest(int chromosomeIndex) {
		if (bestChromosomes.size() == 0) {
			bestChromosomes.add(chromosomeIndex);
			return true;
		} else if (chromosomes.get(chromosomeIndex).getFitness() > chromosomes.get(bestChromosomes.get(bestChromosomes.size() - 1)).getFitness() && !isInBest(chromosomeIndex)) {
			// Find place for new chromosome
			int i = bestChromosomes.size() - 1;
			boolean placeFound = false;
			
			while (i > 0 && !placeFound) {
				// Position found, stop here
				if (chromosomes.get(bestChromosomes.get(i-1)).getFitness() > chromosomes.get(chromosomeIndex).getFitness()) {
					placeFound = true;
				} else {
					i--;
				}				
			}
			// Add chromosome at found place, move all the following elements one place further
			bestChromosomes.add(i, chromosomeIndex);

			// If we overstep the best group size, remove last element
			if (bestChromosomes.size() > bestGroupSize) {
				bestChromosomes.remove(bestChromosomes.size() - 1);
			}
			return true;
		}
		
		return false;
	}

	/**
	 * Check if the parameter chromosome is in the best group
	 * 
	 * @param chromosomeIndex
	 * 		The chromosome to check
	 * @return true if the chromosome is in the best group
	 * 
	 * @see GeneticAlgorithm#bestChromosomes
	 */
	private boolean isInBest(int chromosomeIndex) {
		return bestChromosomes.contains(chromosomeIndex);
	}

	/**
	 * Get one of the best chromosome among the best group.
	 * I.e. the best group first element.
	 * 
	 * @return one of the best schedule in the best group.
	 * 
	 * @see GeneticAlgorithm#bestChromosomes
	 */
	private Schedule getBest() {
		return chromosomes.get(bestChromosomes.get(0));
	}
	
	/**
	 * DAOFactory getter
	 * @return The instance of DAOFactory
	 */
	public DAOFactory getDaoFactory() {
		return daoFactory;
	}
	
	/**
	 * The algorithm final best chromosome getter
	 * @return the final best chromosome
	 */
	public Schedule getFinalBestChromosome() {
		return this.finalBestChromosome;
	}
}