package business;

import business.core.Schedule;
import persistence.DAOFactory;
import persistence.daos.ScheduleDao;
import ui.MainWindowInterface;

/**
 * Implementation of the MainWindowInterface.
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public class MainWindowBusiness implements MainWindowInterface {
	
	private DAOFactory daoFactory;

	/**
	 * MainWindowBusiness constructor.
	 * Retrieves the DAOFactory instance for database interactions.
	 *
	 * @see DAOFactory
	 */
	public MainWindowBusiness() {
		super();
		daoFactory = DAOFactory.getInstance();
	}

	/**
	 * @see Schedule
	 * @see ScheduleDao
	 */
	@Override
	public Schedule findSchedule() {
		// Get Schedule DAO from factory
		ScheduleDao scheduleDao = daoFactory.getScheduleDao();
		Schedule sc = scheduleDao.find();
		return sc;
	}

	/**
	 * @see ScheduleDao
	 */
	@Override
	public void clearSchedule() {
		// Get Schedule DAO from factory
		ScheduleDao scheduleDao = daoFactory.getScheduleDao();
		scheduleDao.clear();		
	}

	@Override
	public void saveSchedule(Schedule finalBestChromosome) {
		// Get Schedule DAO from factory
		ScheduleDao scheduleDao = daoFactory.getScheduleDao();
		scheduleDao.save(finalBestChromosome);
	}

}
