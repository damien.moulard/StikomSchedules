package ui;

import business.core.Schedule;

/**
 * This interface determines what operations have to be implemented for the MainWindowFrames to work properly.
 * 
 * @see MainWindowFrame
 *  
 * @author Damien Moulard
 * @version 1.0
 */
public interface MainWindowInterface {

	/**
	 * Get the schedule stored in database.
	 * 
	 * @return An instance of Schedule.
	 */
	Schedule findSchedule();

	/**
	 * Clear the Schedule table in database.
	 * Only one schedule can be stored at the time.
	 */
	void clearSchedule();

	/**
	 * Save a Schedule in database
	 * 
	 * @param finalBestChromosome
	 * 		The schedule, result of the genetic algorithm, to store.
	 */
	void saveSchedule(Schedule finalBestChromosome);

}
