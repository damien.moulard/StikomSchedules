package business.core;


/**
 * This is the business class that represents a class of a course, performed by a teacher.
 * 
 * A class is identified by :
 * <ul>
 * 		<li>A unique id</li>
 * 		<li>A duration in hours</li>
 * 		<li>A teacher</li>
 * 		<li>A course</li>
 * </ul> 
 * 
 * @see Teacher
 * @see Course
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class CourseClass {
	
	private int id;
	private int duration;
	
	/**
	 * Class teacher
	 * @see Teacher
	 */
	private Teacher teacher;
	
	/**
	 * Class course
	 * @see Course
	 */
	private Course course;
	
	/**
	 * CourseClass constructor
	 * 
	 * @param id
	 * 		Class' id
	 * @param duration
	 * 		Class' duration
	 * @param teacher
	 * 		Class' teacher
	 * @param course
	 * 		Class' course
	 * 
	 * @see CourseClass#teacher
	 * @see CourseClass#course
	 */
	public CourseClass(int id, int duration, Teacher teacher, Course course) {
		super();
		this.id = id;
		this.duration = duration;
		this.teacher = teacher;
		this.course = course;
	}

	/**
	 * ClassId getter
	 * @return the class' id
	 */
	public int getId() {
		return id;
	}

	/**
	 * ClassDuration getter
	 * @return the class' duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * ClassTeacher getter
	 * @return the class' teacher
	 * @see CourseClass#teacher
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * ClassCourse getter
	 * @return the class' course
	 * @see CourseClass#course
	 */
	public Course getCourse() {
		return course;
	}
}
