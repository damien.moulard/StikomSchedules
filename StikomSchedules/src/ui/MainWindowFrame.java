package ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;

import persistence.daoException.DAOException;
import business.GeneticAlgorithm;
import business.SchedulePanelBusiness;
import business.core.Schedule;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This is the main frame of the Stikom Schedules application.
 * It contains an interface that defines the required operations for the frame to be functional.
 * It also contains an instance of the genetic algorithm.
 * 
 * The schedule is displayed by the SchedulePanel panel.
 *  
 * @see MainWindowInterface
 * @see SchedulePanel
 * @see GeneticAlgorithm
 * @see Schedule
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class MainWindowFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5066710121929744989L;
	
	private JPanel contentPane;
	private SchedulePanel schedulePanel;
	private JScrollPane scrollPane;

	private MainWindowInterface business;
	private GeneticAlgorithm algo;
	private Schedule schedule;
	
	/**
	 * Create the frame.
	 * 
	 * @param mainWindowBusiness
	 * 		The business class of this frame, implementation of MainWindowInterface
	 * @param algorithm
	 * 		The genetic algorithm
	 */
	public MainWindowFrame(MainWindowInterface mainWindowBusiness, GeneticAlgorithm algorithm) {
		this.business = mainWindowBusiness;
		this.algo = algorithm;
		
		// JFrame initialization
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Stikom Schedules");
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		// Add the Generate Schedule button and its listener
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnGenerateSchedule = new JButton("Generate Schedule");
		btnGenerateSchedule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					generatePressed();
				} catch (DAOException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		panel.add(btnGenerateSchedule);
		
		// Create the JScrollPane that will contain the SchedulePanel
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		// Get Schedule table content
		schedule = business.findSchedule();
		
		// If no schedule saved
		if (schedule == null) {
			JPanel noSchedulePanel = new JPanel();
			scrollPane.setViewportView(noSchedulePanel);
			
			JLabel lblNoScheduleSaved = new JLabel("No schedule saved.");
			noSchedulePanel.add(lblNoScheduleSaved);
		} else {
			// Calculate fitness of the schedule
			algo.calculateFitness(schedule);
			
			// Display the schedule
			schedulePanel = new SchedulePanel(new SchedulePanelBusiness(), schedule);
			scrollPane.setViewportView(schedulePanel);
			
			//schedule.toString();
		}
	}

	/**
	 * Action triggered when Generate button is pressed
	 */
	private void generatePressed() {
		// If a schedule is already stored in database
		if (schedule != null) {
			// Warning message
			int choice = JOptionPane.showConfirmDialog(
							null,
							"The old schedule will be removed. Proceed?",
							"Warning", JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
			if (choice == 0) {
				// Clear Schedule table
				business.clearSchedule();
				runGeneticAlgorithm();
			}
		} else {
			runGeneticAlgorithm();
		}	
	}
	
	/**
	 * Launch the genetic algorithm
	 */
	private void runGeneticAlgorithm() {
		// Run the genetic algorithm
		algo.run();
		
		// Save the result
		schedule = algo.getFinalBestChromosome();
		business.saveSchedule(schedule);	
		
		// If a SchedulePanel already displayed
		if (schedulePanel != null) {
			contentPane.remove(schedulePanel);
		}
		
		// Display schedule
		schedulePanel = new SchedulePanel(new SchedulePanelBusiness(), schedule);
		scrollPane.setViewportView(schedulePanel);
		validate();
		
		//schedule.toString();
	}
}
