package persistence.daoException;

/**
 * DAO exception type created to hide specific exceptions that occur when running.
 * This exception type concerns the database interaction.
 * 
 * @author Damien Moulard
 * @version 1.0
 */
public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 8927149926524034299L;

	public DAOException(String message) {
        super(message);
    }

    public DAOException( String message, Throwable cause ) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
