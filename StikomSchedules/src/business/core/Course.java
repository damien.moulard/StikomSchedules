package business.core;


/** 
 * This is the business class that represents a course.
 *  
 * A course is identified by an unique id and a name.
 * 
 * @author Damien Moulard
 * @version 1.0
 * */
public class Course {

	private int id;
	private String name;
	
	/**
	 * Course constructor
	 * 
	 * @param id
	 * 		Course's id
	 * @param name
	 * 		Course's name
	 */
	public Course(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	/**
	 * CourseId getter
	 * @return the course's id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * CourseName getter
	 * @return the course's name
	 */
	public String getName() {
		return name;
	}
}
